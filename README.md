# OpenML dataset: leukemia

https://www.openml.org/d/1104

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: Unknown - Date unknown  
**Please cite**:   

Molecular Classification of Cancer: Class Discovery and Class Prediction by Gene Expression Monitoring.

Science, VOL 286, pp. 531-537, 15 October 1999.
Web supplement to the article

T.R. Golub, D. K. Slonim, P. Tamayo, C. Huard, M. Gaasenbeek, J. P. Mesirov, H. Coller, M. L. Loh, J. R. Downing, M. A. Caligiuri, C. D. Bloomfield, E. S. Lander.

Original training test consists of 38 first instances.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/1104) of an [OpenML dataset](https://www.openml.org/d/1104). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/1104/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/1104/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/1104/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

